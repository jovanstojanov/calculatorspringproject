<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<header><title>Calculator Spring Project</title></header>
<style>
    .error {
        color: red;
    }
    .form-radio
    {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        display: inline-block;
        position: relative;
        background-color: #f1f1f1;
        color: #666;
        top: 10px;
        height: 30px;
        width: 30px;
        border: 0;
        border-radius: 50px;
        cursor: pointer;
        margin-right: 7px;
        outline: none;
    }
    .form-radio:checked::before
    {
        position: absolute;
        font: 13px/1 'Open Sans', sans-serif;
        left: 11px;
        top: 7px;
        content: '\02143';
        transform: rotate(40deg);
    }
    .form-radio:hover
    {
        background-color: #f7f7f7;
    }
    .form-radio:checked
    {
        background-color: #f1f1f1;
    }
    label
    {
        font: 300 16px/1.7 'Open Sans', sans-serif;
        color: #666;
        cursor: pointer;
    }
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 14px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .button4 {background-color: #008CBA;}
    
</style>
<body>
<form id="calculate">
    <p>Please enter two numbers:</p>
    <div>
        <label for="number1">Number 1</label>
        <input type="number" step="any" name="number1" id="number1"/>
        <label id="number1-error" class="error"></label>
    </div>
    <div>
        <label for="number2">Number 2</label>
        <input type="number" step="any" name="number2" id="number2"/>
        <label id="number2-error" class="error"></label>
    </div>
    <div>
        <label for="result" style="padding-right: 25px;">Result</label>
        <input readonly name="result" id="result"/>
    </div>
    <p>Choose desired operation:</p>
    <input type="radio" id="rbadd" value="add" class="form-radio" name="operation" checked> Addition<br>
    <input type="radio" id="rbsub" value="sub" class="form-radio" name="operation"> Subtraction<br>
    <input type="radio" id="rbmul" value="mul" class="form-radio" name="operation"> Multiplication<br>
    <input type="radio" id="rbdiv" value="div" class="form-radio" name="operation"> Division<br><br>
    <input id="calculateButton" type="submit" value="Calculate" class="button button4"/>
    <label id="global-error" class="error"></label>
</form>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script>

    function getRestEndpoint() {
        return $('input[name=operation]:checked').val();
    }

    $(document).ready(function () {
        $("#calculate").submit(function (event) {
            event.preventDefault();
            fire_ajax_submit();
        });
    });

    function fire_ajax_submit() {

        var formData = {};
        formData["number1"] = $("#number1").val();
        formData["number2"] = $("#number2").val();
        formData["action"] = getRestEndpoint();

        $("#calculateButton").prop("disabled", true);
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: getRestEndpoint(),
            data: JSON.stringify(formData),
            dataType: 'json',
            cache: false,
            timeout: 6000,
            success: function (data) {
                $(".error").html("");
                $("#result").val(data);

                $("#calculateButton").prop("disabled", false);
            },
            error: function (e) {
                $(".error").html("");
                $("#result").val("");
                var errorResponseJson = JSON.parse(e.responseText);
                for (var i = 0; i < errorResponseJson.errorModels.length; i++) {
                    $("#" + errorResponseJson.errorModels[i].field + "-error").append(errorResponseJson.errorModels[i].message).append("<br/>");
                }
                $("#calculateButton").prop("disabled", false);
            }
        });

    }
</script>
</body>
</html>