package com.example.demo.service.implementation;

import com.example.demo.service.CalculatorService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public Double add(Double x, Double y) {
        return x + y;
    }

    @Override
    public Double mul(Double x, Double y) {
        return x * y;
    }

    @Override
    public Double div(Double x, Double y) {
        return x / y;
    }

    @Override
    public Double sub(Double x, Double y) {
        return x - y;
    }
}
