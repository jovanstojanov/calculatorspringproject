package com.example.demo.service;

public interface CalculatorService {

    public Double add(Double x, Double y);

    public Double sub(Double x, Double y);

    public Double mul(Double x, Double y);

    public Double div(Double x, Double y);
}
