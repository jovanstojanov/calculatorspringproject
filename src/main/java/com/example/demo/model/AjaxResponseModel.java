package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

public class AjaxResponseModel {

    List<ErrorModel> errorModels;
    Double result;

    public AjaxResponseModel() {
        errorModels = new ArrayList<>();
    }

    public void addErrorModel(ErrorModel errorModel) {
        errorModels.add(errorModel);
    }

    public List<ErrorModel> getErrorModels() {
        return errorModels;
    }

    public void setErrorModels(List<ErrorModel> errorModels) {
        this.errorModels = errorModels;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }
}