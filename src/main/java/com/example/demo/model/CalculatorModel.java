package com.example.demo.model;

import com.example.demo.validator.DivideByZero;

import javax.validation.constraints.NotNull;

@DivideByZero(
        field = "number2",
        action = "action"
)
public class CalculatorModel {

    @NotNull(message = "Please enter value for operand 1")
    private Double number1;

    @NotNull(message = "Please enter value for operand 2")
    private Double number2;

    @NotNull(message = "Action is missing.")
    private String action;

    public CalculatorModel() {
    }

    public CalculatorModel(Double number1, Double number2, String action) {
        this.number1 = number1;
        this.number2 = number2;
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Double getNumber1() {
        return number1;
    }

    public void setNumber1(Double number1) {
        this.number1 = number1;
    }

    public Double getNumber2() {
        return number2;
    }

    public void setNumber2(Double number2) {
        this.number2 = number2;
    }
}
