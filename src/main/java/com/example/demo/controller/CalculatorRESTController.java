package com.example.demo.controller;

import com.example.demo.model.AjaxResponseModel;
import com.example.demo.model.CalculatorModel;
import com.example.demo.model.ErrorModel;
import com.example.demo.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CalculatorRESTController {

    @Autowired
    private CalculatorService calcService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addOperation(@Valid @RequestBody CalculatorModel calculatorModel, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(getErrorResponseModel(errors));
        }

        Double result = calcService.add(calculatorModel.getNumber1(), calculatorModel.getNumber2());
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/sub", method = RequestMethod.POST)
    public ResponseEntity<?> subOperation(@Valid @RequestBody CalculatorModel calculatorModel, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(getErrorResponseModel(errors));
        }

        Double result = calcService.sub(calculatorModel.getNumber1(), calculatorModel.getNumber2());
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/mul", method = RequestMethod.POST)
    public ResponseEntity<?> mulOperation(@Valid @RequestBody CalculatorModel calculatorModel, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(getErrorResponseModel(errors));
        }

        Double result = calcService.mul(calculatorModel.getNumber1(), calculatorModel.getNumber2());
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/div", method = RequestMethod.POST)
    public ResponseEntity<?> divOperation(@Valid @RequestBody CalculatorModel calculatorModel, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(getErrorResponseModel(errors));
        }
        Double result = calcService.div(calculatorModel.getNumber1(), calculatorModel.getNumber2());
        return ResponseEntity.ok(result);
    }

    private AjaxResponseModel getErrorResponseModel(Errors errors) {
        AjaxResponseModel ajaxResponseModel = new AjaxResponseModel();
        for (ObjectError error : errors.getAllErrors()) {
            String fieldName = "global";
            if (error.getArguments() != null) {
                Object field = error.getArguments()[error.getArguments().length - 1];
                fieldName = ((MessageSourceResolvable) field).getDefaultMessage();
            }
            ajaxResponseModel.addErrorModel(new ErrorModel(error.getDefaultMessage(), fieldName));
        }
        return ajaxResponseModel;
    }
}
