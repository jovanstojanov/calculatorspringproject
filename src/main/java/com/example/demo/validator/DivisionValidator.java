package com.example.demo.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DivisionValidator implements ConstraintValidator<DivideByZero, Object> {

    private String field;
    private String action;

    public void initialize(DivideByZero constraintAnnotation) {
        this.field = constraintAnnotation.field();
        this.action = constraintAnnotation.action();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {

        Object fieldValue = new BeanWrapperImpl(value)
                .getPropertyValue(field);
        Object actionValue = new BeanWrapperImpl(value)
                .getPropertyValue(action);

        if (actionValue != null && actionValue.equals("div") && fieldValue != null && (Double) fieldValue == 0) {
            return false;
        }
        return true;
    }
}