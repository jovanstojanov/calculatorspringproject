package com.example.demo.validator;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = DivisionValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DivideByZero {

    String message() default "Division by zero not allowed!";

    String field();

    String action();

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}